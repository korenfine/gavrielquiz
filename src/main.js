// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router/index'
import axios from 'axios'
import BootstrapVue from 'bootstrap-vue'
import '@/assets/global.styl'

Vue.use(BootstrapVue)

Vue.config.productionTip = false

const serverUrl = 'http://localhost:8070'
Vue.prototype.$axios = axios.create({
  baseURL: `${serverUrl}/api`
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
