const express = require('express')
const app = express()
const port = 8070

const cors = require('cors')
app.use(cors())

const fs = require('fs')
const data = JSON.parse(fs.readFileSync(__dirname + '/data.json', 'utf8'))

app.listen(port, () => console.log(`app listening on port ${port}!`))

// api
app.get('/api/getData/:year', (req, res) => {
  // get specific data
  const currentData = data.data.find(d => parseInt(d.year) === parseInt(req.params.year))

  // send data
  res.send(currentData)
})
